package com.customer;

public class DummyCustomerStore implements CustomerStore {
    
    static final String[] names = { "Customer One", "Customer Two", "Customer Three", "Customer Four", "Customer Five" };
    
    private Customer[] allCustomers = new Customer[0];
    
    private Customer[] selectedCustomers = new Customer[0];
    
    public DummyCustomerStore() {
        init();
    }
    
    private void init() {
        allCustomers = new Customer[5];
        for (int i = 0; i < allCustomers.length; i++) {
            allCustomers[i] = new Customer();
            allCustomers[i].setCustomerId(1001 + i);
            allCustomers[i].setFullName(names[i]);
        }
    }
    
    public void setSelectedCustomers(Customer[] sCustomers) {
        if (sCustomers == null)
            throw new IllegalArgumentException("customers' array cannot be null");
        selectedCustomers = sCustomers;
    }
    
    public Customer[] getSelectedCustomers() {
        return selectedCustomers;
    }
    
    public Customer[] getAllCustomers() {
        return allCustomers;
    }
}

package com.customer;

public interface CustomerStore {

    public Customer[] getSelectedCustomers();

    public void setSelectedCustomers(Customer[] selectedCustomers);

    public Customer[] getAllCustomers();
}

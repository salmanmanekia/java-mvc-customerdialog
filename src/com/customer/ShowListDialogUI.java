package com.customer;

import java.awt.Dimension;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class ShowListDialogUI extends JFrame {
    JTextArea DisplayedCustomerList ;
    
    public ShowListDialogUI () {
        setTitle ("Show List");
        DisplayedCustomerList = new JTextArea();
        DisplayedCustomerList.setLineWrap(true);
        DisplayedCustomerList.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        DisplayedCustomerList.setEditable(false);
        add(new JScrollPane(DisplayedCustomerList));
        
        setSize(new Dimension(350, 300));
        
        setLocationRelativeTo(null);
        setVisible(false);
    }
    
    public void displayCustomer (List<Customer> customerList) {
        getContentPane().removeAll();
        setContentPane(new JScrollPane(DisplayedCustomerList));
        setVisible(true);
        for (Customer customer: customerList) { 
            DisplayedCustomerList.append("Customer-ID : " + customer.getCustomerId() + " " 
                                             + "Customer Name : " + customer.getFullName() + 
                                         System.getProperty( "line.separator" )); 
        } 
        DisplayedCustomerList = new JTextArea();
    }
}
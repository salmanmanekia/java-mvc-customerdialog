package com.customer;

public class MainClass {
    public static void main (String[] args) {       
        DummyCustomerStore dCStore = new DummyCustomerStore();
        TestApplicationUI testAppUI = new TestApplicationUI();
        new Controller(dCStore, testAppUI);
    }    
}
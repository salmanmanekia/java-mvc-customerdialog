package com.customer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JScrollPane;

public class Controller implements ActionListener, WindowListener {
    
    TestApplicationUI appUI;
    CustomerSelectorDialogUI selectUI;
    ShowListDialogUI showListUI;
    DummyCustomerStore store;
    List<Customer> displayedCustomerList;
    boolean checkBoxRecord [];
    int numOfCustomers = 0;
    
    public Controller( DummyCustomerStore store, TestApplicationUI appUI ) {
        numOfCustomers = store.getAllCustomers().length;
        checkBoxRecord = new boolean[numOfCustomers];
        displayedCustomerList =new ArrayList<Customer>();
        selectUI = new CustomerSelectorDialogUI();
        showListUI = new ShowListDialogUI();
        this.store = store;
        this.appUI = appUI;
        appUI.ButtonListener( this );  
        selectUI.addWindowListener(this);
    } 
    
    @Override
    public void actionPerformed(ActionEvent event) {
        String viewAction = event.getActionCommand();
        
        if (viewAction.equals("TEST")) {
            selectUI.PopulateAndShow(store);
            selectUI.actionListeners(new CustomerSelectorUIListener(this, selectUI, showListUI ) );
            selectUI.setVisible( true );
        }
        else if (viewAction.equals("SHOW-LIST")) {
            showListUI.displayCustomer(displayedCustomerList);
        }
    }
    
    public void applyButtonClicked() {
        
        int row = 0;
        Customer []oldCustomerArray = store.getSelectedCustomers();
        ArrayList<Customer> customers = new ArrayList<Customer>();
        for(Customer customer : store.getAllCustomers())
            if (selectUI.getBoolean(row++)) {
            customers.add(customer);
        }
        Customer[] newCustomerArray = new Customer[customers.size()];
        newCustomerArray = customers.toArray(newCustomerArray);
        
        store.setSelectedCustomers(newCustomerArray);
        List<Customer> oldCustomerList = new ArrayList(Arrays.asList(oldCustomerArray));
        displayedCustomerList = new ArrayList(Arrays.asList(newCustomerArray));
        displayedCustomerList.removeAll(oldCustomerList);
    }
    
    @Override
    public void windowActivated(WindowEvent arg0) {}
    
    @Override
    public void windowClosed(WindowEvent arg0) {}
    
    @Override
    public void windowClosing(WindowEvent arg0) {
        if (showListUI.isShowing()) {
            showListUI.setVisible(false);
        }
    }
    
    @Override
    public void windowDeactivated(WindowEvent arg0) {}
    
    @Override
    public void windowDeiconified(WindowEvent arg0) {}
    
    @Override
    public void windowIconified(WindowEvent arg0) {}
    
    @Override
    public void windowOpened(WindowEvent arg0) {}
    
}


class CustomerSelectorUIListener implements ActionListener {
    
    CustomerSelectorDialogUI custSelectView;
    Controller control;
    ShowListDialogUI showListUI;
    
    public CustomerSelectorUIListener (Controller control, CustomerSelectorDialogUI custSelectView, ShowListDialogUI showListUI) {
        this.custSelectView = custSelectView;
        this.control = control;
        this.showListUI = showListUI;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionEvent = e.getActionCommand();
        
        if ( actionEvent.equals( "applyButton" ) )
        {
            control.applyButtonClicked();
        }
        else if ( actionEvent.equals( "cancelButton" ) )
        {
            custSelectView.setVisible( false );
            if (showListUI.isShowing()) {
                showListUI.setVisible(false);
            }
        }  
        else if ( actionEvent.equals( "clearAllButton" ) )
        {
            custSelectView.toggleSelection(false);
        }  
        else if ( actionEvent.equals( "selectAllButton" ) )
        {
            custSelectView.toggleSelection(true);
        }
    }
}
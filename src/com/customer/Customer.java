package com.customer;

public class Customer {
    
    public int customerId;
    public String fullName;
    public String firstName;
    public String lastName;
    public int portfolioManager = 0;
    public int person = 0;
    
    public Customer() { }
    
    public boolean equals(Object o) {
        boolean isEqual = false;
        if(o != null || ( o instanceof  Customer)) {
            Customer other = (Customer) o;
            if(other.getCustomerId() == this.getCustomerId())
                isEqual = true;
        }
        return isEqual;
    }
    
    protected String valuesToString() {
        return "customerId= "+ customerId +
            ", firstName= "+ firstName +
            ", lastName= "+ lastName +
            ", fullName= "+fullName +
            ", portfolioMgr="+ (portfolioManager == 1); 
    }
    
    public String toString() {
        return "StructCustomer - [" + valuesToString() + "]";
    }
    
    public int getCustomerId() {
        return customerId;
    }    
    
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    
    public java.lang.String getFullName() {
        return fullName;
    }
    
    public void setFullName(java.lang.String fullName) {
        this.fullName = fullName;
    }
   
    public java.lang.String getLastName() {
        return lastName;
    }
    
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }
    
    public java.lang.String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(java.lang.String firstname) {
        this.firstName = firstname;
    }
    
    public void setPortfolioManager(boolean pfMgr) {
        if(pfMgr)
            this.portfolioManager = 1;
        else
            this.portfolioManager = 0;
    }
    
    public boolean isPortfolioManager() {
        return (this.portfolioManager == 1);
    }
    
    public void setPerson(boolean person) {
        this.person = (person == true ?  1 : 0);
    }
    
    public boolean isPerson() {
        return (this.person == 1);
    }
    
    public int compareTo(Object o) {
        return this.fullName.compareTo(((Customer) o).fullName);
    }
    
}

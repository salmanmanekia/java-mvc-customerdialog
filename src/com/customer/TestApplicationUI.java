package com.customer;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TestApplicationUI extends JFrame {
    
    JPanel mainGUI;
    JButton testButton;
    JButton showListButton;
    
    public TestApplicationUI() {
        setTitle("Main Dialog");
        
        setLocationRelativeTo(null);
        setSize(300, 110);
        setResizable(false);
        setVisible(true);
        mainGUI = new JPanel();
        testButton = new JButton("TEST");
        showListButton = new JButton("SHOW-LIST");
        setContentPane(createContentPane());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    } 
    
    private JPanel createContentPane() {
        mainGUI.setLayout(null);
        
        testButton.setLocation(10, 10);
        testButton.setSize(130, 60);
        mainGUI.add(testButton);
        
        showListButton.setLocation(150, 10);
        showListButton.setSize(130, 60);
        mainGUI.add(showListButton);
        
        return mainGUI;
    }
    
    protected void ButtonListener( ActionListener cont ) {
        testButton.addActionListener( cont );
        showListButton.addActionListener( cont );
    }
}
package com.customer;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

public class CustomerSelectorDialogUI extends JFrame {
    private final int COLUMN_COUNT = 3;
    
    private String applyButtonName = "applyButton";
    private String cancelButtonName = "cancelButton";
    private String clearAllButtonName = "clearAllButton";
    private String selectAllButtonName = "selectAllButton";
    
    JPanel custSelectPanel, buttonPanel;
    
    TableModel tModel;
    tableModelListener tML;
    JTable customerTable;
    JScrollPane scrollPane;
    
    JButton applyButton;
    JButton cancelButton;
    JButton clearAllButton;
    JButton selectAllButton;
    
    boolean[] checkBox;
    
    public CustomerSelectorDialogUI() {
        
        setTitle("Customer Selection Dialog");
        custSelectPanel = new JPanel();
        buttonPanel = new JPanel();
        selectAllButton = new JButton(" Select All ");
        clearAllButton = new JButton(" Clear All ");
        applyButton = new JButton(" Apply ");
        cancelButton = new JButton(" Cancel "); 
    }
    
    protected void actionListeners( ActionListener actList ) {
        applyButton.setActionCommand( applyButtonName );
        applyButton.addActionListener( actList );
        
        cancelButton.setActionCommand( cancelButtonName );
        cancelButton.addActionListener( actList );
        
        clearAllButton.setActionCommand( clearAllButtonName );
        clearAllButton.addActionListener( actList ); 
        
        selectAllButton.setActionCommand( selectAllButtonName );
        selectAllButton.addActionListener( actList ); 
    }
    
    public boolean getBoolean (int row) {
        return tML.getBoolean(row);
    }
    
    public void toggleSelection(Boolean select) {
        for (int rowIndex = 0; rowIndex < tModel.getRowCount(); rowIndex++) {
            tModel.setValueAt(select, rowIndex, 0);
        }
    }
    
    public void PopulateAndShow(DummyCustomerStore dCStore) {
        
        List<Object[]> data = new ArrayList<Object[]>();
        int numberOfCustomers = dCStore.getAllCustomers().length;
        checkBox = new boolean[numberOfCustomers];
        for (Customer customer : dCStore.getAllCustomers()) {
            Object record[] = new Object[COLUMN_COUNT];
            record[0] = Boolean.FALSE;
            record[1] = Integer.toString(customer.customerId);
            record[2] = customer.fullName;
            data.add(record);
        }  
        tModel = new TableModel(data);
        customerTable = new JTable(tModel);
        scrollPane = new JScrollPane(customerTable);
        tML = new tableModelListener(numberOfCustomers);
        customerTable.getModel().addTableModelListener(tML);
        
        setContentPane(createContentPane());
        
        setSize(480, 580);
        setResizable(false);
        setVisible(true);
    }
    
    private JPanel createContentPane() {
        custSelectPanel.setLayout(null);
        
        customerTable.setDragEnabled(false);
        customerTable.setFillsViewportHeight(true);
        
        scrollPane.setLocation(10, 10);
        scrollPane.setSize(450,450);
        
        custSelectPanel.add(scrollPane);
        
        buttonPanel.setLayout(null);
        buttonPanel.setLocation(10, 480);
        buttonPanel.setSize(450, 100);
        custSelectPanel.add(buttonPanel);
        
        selectAllButton.setLocation(0, 0);
        selectAllButton.setSize(100, 40);
        buttonPanel.add(selectAllButton);
        
        
        clearAllButton.setLocation(110, 0);
        clearAllButton.setSize(100, 40);
        buttonPanel.add(clearAllButton);
        
        applyButton.setLocation(240, 0);
        applyButton.setSize(100, 40);
        buttonPanel.add(applyButton);
        
        cancelButton.setLocation(350, 0);
        cancelButton.setSize(100, 40);
        buttonPanel.add(cancelButton);
        
        return custSelectPanel;
    }
    
    public class tableModelListener implements TableModelListener {
        
        boolean [] checkBoxValue;
        public tableModelListener(int rows) {
            checkBoxValue = new boolean[rows];
        }
        
        public void setBoolean (int row, boolean value) {
            checkBox[row] = value;  
        }
        
        public boolean getBoolean (int row) {
            return checkBox[row];
        }
        
        @Override
        public void tableChanged(TableModelEvent e) {
            int row = e.getFirstRow();
            int column = e.getColumn();
            TableModel model = (TableModel)e.getSource();
            Boolean data = (Boolean) model.getValueAt(row, column);
            setBoolean(row, data);
        }
    }
    
    private class TableModel extends AbstractTableModel {
        
        private List<Object[]> data;
        public TableModel(List<Object[]> data) {
            this.data = data;
        }
        
        private String[] columnNames = {"Selected ",
            "Customer Id ",
            "Customer Name "
        };
        
        public int getColumnCount() {
            return COLUMN_COUNT;
        }
        public int getRowCount() {
            return data == null ? 0 : data.size();
        }
        
        public String getColumnName(int col) {
            return columnNames[col];
        }
        
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
            getRecord(rowIndex)[columnIndex] = value;
            super.fireTableCellUpdated(rowIndex, columnIndex);
        }
        
        private Object[] getRecord(int rowIndex) {
            return (Object[]) data.get(rowIndex);
        }
        
        public Object getValueAt(int rowIndex, int columnIndex) {
            return getRecord(rowIndex)[columnIndex];
        }
        
        public Class<?> getColumnClass(int columnIndex) {
            if (data == null || data.size() == 0) {
                return Object.class;
            }
            Object o = getValueAt(0, columnIndex);
            return o == null ? Object.class : o.getClass();
        }
        
        public boolean isCellEditable(int row, int col) {
            if (col > 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
